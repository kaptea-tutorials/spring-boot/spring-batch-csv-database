package com.kaptea.employee.batch;

import com.kaptea.employee.entity.Employee;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class EmployeeProcessor implements ItemProcessor<Employee, Employee> {


    @Override
    public Employee process(Employee employee) throws Exception {
        return employee;
    }
}