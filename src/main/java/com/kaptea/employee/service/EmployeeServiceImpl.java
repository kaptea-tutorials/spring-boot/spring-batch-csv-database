package com.kaptea.employee.service;

import com.kaptea.employee.dto.UserDto;
import com.kaptea.employee.repository.EmployeeRepository;
import com.kaptea.employee.entity.Employee;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        super();
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee findByUsernameAndPassword(UserDto userDto) {
        return employeeRepository.findByUsernameAndPasswordAndActive(userDto.getUsername(), userDto.getPassword(), true);
    }
}