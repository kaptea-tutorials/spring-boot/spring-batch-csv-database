package com.kaptea.employee.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name="qualification")
public class Qualification implements Serializable{
	
	private static final long serialVersionUID = -8477514131515106343L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(length=80)
	private String course;
	
	@Column(length=20)
	private String courceType;
	
	@Column(length=50)
	private String branch;
	
	@Column
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern ="yyyy-MM-dd")
	private Date startDate;
	
	@Column
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern ="yyyy-MM-dd")
	private Date yearOfCompletion;
	
	@Column(length=10)
	private double aggregate;
    
    @JsonBackReference
    @ApiModelProperty(hidden=true)
	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employee employee;
		
	public Qualification(){}
}