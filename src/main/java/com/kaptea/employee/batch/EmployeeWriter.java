package com.kaptea.employee.batch;

import com.kaptea.employee.entity.Employee;
import com.kaptea.employee.repository.EmployeeRepository;
import com.kaptea.employee.service.EmployeeService;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmployeeWriter implements ItemWriter<Employee> {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void write(List<? extends Employee> employees) throws Exception {
        employeeRepository.save(employees);
    }
}